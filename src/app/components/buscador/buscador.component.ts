import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
})
export class BuscadorComponent implements OnInit {

  heroes: any[] = [];
  termino: string;

  constructor(private _activatedRouter: ActivatedRoute, private _heroesService: HeroesService) { }

  ngOnInit() {
    this._activatedRouter.params.subscribe(params => {
      this.termino = params['termino'];
      this.heroes = this._heroesService.searchHeroes(params['termino']);
    });
  }

}
