import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../../services/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
})
export class NavigationComponent implements OnInit {
  constructor(private _router: Router) { }

  ngOnInit() {
  }

  searchHeroe(termino: string) {
    this._router.navigate(['/buscar', termino]);
  }

}
